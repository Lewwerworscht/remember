#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define NOTEDIR_PATH "/.notes/"
#define MAXPATHLEN   255
#define FNAMELEN     16
#define TMPNAME      "/newnote"

void edit_notes(char fpath[], char *editor);
void find_notes(int argc, char *argv[]);
char *get_editor_name(int argc, char *argv[], int tagc);
char *get_note_path(struct tm *ntime);
void new_note(int argc, char *argv[], struct tm *ntime);
void print_note_header(FILE *f, int hour, int minute, int tagc, char *tagv[]);
void usage(char appname[]);
void write_note_arg(char fpath[], int hour, int minute, int argc, char *argv[], int tagc, char *tagv[]);
void write_note_editor(char fpath[], char *editor, int hour, int minute, int tagc, char *tagv[]);



int
main(int argc, char *argv[])
{
	time_t rawtime;
	struct tm *ntime;
	time(&rawtime);
	ntime = localtime(&rawtime);

	if (argc < 2)
		usage(argv[0]);

	if (!strcmp(argv[1], "help") ||
	    !strcmp(argv[1], "--help") ||
	    !strcmp(argv[1], "-h") ||
	    !strcmp(argv[1], "h")) {
		usage(argv[0]);
	} else if (!strcmp(argv[1], "edit") ||
	           !strcmp(argv[1], "e")) {
		char *fpath = get_note_path(ntime);
		char *editor = get_editor_name(argc, argv, 1);
		edit_notes(fpath, editor);
	} else if (!strcmp(argv[1], "find") ||
	           !strcmp(argv[1], "f")) {
		find_notes(argc, argv);
	} else if (!strcmp(argv[1], "new") ||
	           !strcmp(argv[1], "n")) {
		new_note(argc, argv, ntime);
	} else {
		usage(argv[0]);
	}

	return 0;
}



void
edit_notes(char fpath[], char *editor)
{
	pid_t pid = fork();
	int wstatus;
	switch (pid) {
		case -1: {
			perror("fork");
			exit(EXIT_FAILURE);
		} break;
		case 0: {
			/* NOTE: Why doesn't it work with { ... } inside execvp? */
			char *editorcmd[] = {editor, fpath, NULL};
			execvp(editorcmd[0], editorcmd);
			/* if execvp fails */
			perror("execvp");
			exit(EXIT_FAILURE);
		} break;
		default: {
			/* TODO: waitpid (?) */
			wait(&wstatus);
			if (WIFEXITED(wstatus) && WEXITSTATUS(wstatus))
				exit(EXIT_FAILURE);
		}
	}
}

void
find_notes(int argc, char *argv[])
{
	printf("NOT IMPLEMENTED\n");
	exit(EXIT_FAILURE);
}

char *
get_editor_name(int argc, char *argv[], int argpos)
{
	/*
         * argpos gives the position of the arg defining that an editor is needed.
         * This has to be the last arg, or second to last if an editor arg is given.
         */
	char *editor;

	/* Only called if -e or -E flag is given */
	if (argc == argpos+1) {
		editor = getenv("EDITOR");
		if (editor == NULL) {
			fprintf(stderr, "$EDITOR not set.\n");
			exit(EXIT_FAILURE);
		}
	} else if (argc == argpos+2) {
		editor = argv[argpos+1];
	} else {
		usage(argv[0]);
	}

	return editor;
}

char *
get_note_path(struct tm *ntime)
{
	static char fpath[MAXPATHLEN];
	char *homedir = getenv("HOME");
	char fname[FNAMELEN];
	sprintf(fname, "%.4d-%.2d-%.2d.note", ntime->tm_year+1900, ntime->tm_mon+1, ntime->tm_mday);

	if (strlen(homedir)+strlen(NOTEDIR_PATH)+FNAMELEN >= MAXPATHLEN) {
		fprintf(stderr, "Maximal path length to note file too short.\n");
		exit(EXIT_FAILURE);
	} else {
		strncpy(fpath, homedir, strlen(homedir)+1);
		strncat(fpath, NOTEDIR_PATH, strlen(NOTEDIR_PATH)+1);
		strncat(fpath, fname, FNAMELEN+1);
	}

	return fpath;
}

void
new_note(int argc, char *argv[], struct tm *ntime)
{
	int tagc = 0;
	char *tagv[argc-2];
	for (int i=0; i<argc-2; i++) {
		if (argv[i+2][0] == '+') {
			tagv[i] = argv[i+2]+1;
			tagc++;
		} else {
			break;
		}
	}

	if (argc - tagc < 3) {
		usage(argv[0]);
	}

	char *fpath = get_note_path(ntime);
	if (!strcmp(argv[2+tagc], "-e")) {
		char *editor = get_editor_name(argc, argv, 2+tagc);
		write_note_editor(fpath, editor, ntime->tm_hour, ntime->tm_min, tagc, tagv);
	} else {
		write_note_arg(fpath, ntime->tm_hour, ntime->tm_min, argc, argv, tagc, tagv);
	}
}

void
print_note_header(FILE *f, int hour, int minute, int tagc, char *tagv[])
{
	fprintf(f, "\n--- %.2d:%.2d ---", hour, minute);
	for (int i=0; i<tagc; i++) {
		fprintf(f, " %s", tagv[i]);
	}
	fprintf(f, "\n");
}

void
usage(char appname[])
{
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "  %s n[ew] [+tag ...] (<note> | -e [editor])\n", appname);
	fprintf(stderr, "  %s e[dit] [editor]\n", appname);
	fprintf(stderr, "  %s f[ind] tag [tag ...] [findoptions]\n", appname);

	fprintf(stderr, "\nfindoptions:\n");
	fprintf(stderr, "  -y year [year ...]\n");
	fprintf(stderr, "  -m month [month ...]\n");
	fprintf(stderr, "  -d day [day ...]\n");
	exit(EXIT_FAILURE);
}

void
write_note_arg(char fpath[], int hour, int minute, int argc, char *argv[], int tagc, char *tagv[])
{
	FILE *f = fopen(fpath, "a");
	if (f == NULL) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	print_note_header(f, hour, minute, tagc, tagv);
	for (int i=2+tagc; i<argc-1; i++)  {
		fprintf(f, "%s ", argv[i]);
	}
	fprintf(f, "%s\n", argv[argc-1]);
	fclose(f);
}

void
write_note_editor(char fpath[], char *editor, int hour, int minute, int tagc, char *tagv[])
{
	/* TODO: Can this be done in a simpler way? */

	/* NOTE: Why does this work with a char array but not a char pointer? */
	char tmppath[MAXPATHLEN];
	/* TODO: Change /tmp to C_tmpdir */
	char tmptemplate[] = "/tmp/remember.XXXXXX";
	char *tmpdir = mkdtemp(tmptemplate);
	if (tmpdir == NULL) {
		perror("mkdtemp");
		exit(EXIT_FAILURE);
	}

	if (strlen(tmpdir)+strlen(TMPNAME) >= MAXPATHLEN) {
		fprintf(stderr, "Maximal path length to temporary file too short.\n");
		exit(EXIT_FAILURE);
	} else {
		strncpy(tmppath, tmpdir, strlen(tmpdir)+1);
		strncat(tmppath, TMPNAME, strlen(TMPNAME)+1);
	}

	pid_t pid = fork();
	int wstatus;
	switch (pid) {
		case -1: {
			remove(tmppath);
			remove(tmpdir);
			perror("fork");
			exit(EXIT_FAILURE);
		} break;
		case 0: {
			char *editorcmd[] = {editor, tmppath, NULL};
			execvp(editorcmd[0], editorcmd);
			/* if execvp fails */
			perror("execvp");
			exit(EXIT_FAILURE);
		} break;
		default: {
			wait(&wstatus);
			if (WIFEXITED(wstatus) && WEXITSTATUS(wstatus))
				exit(EXIT_FAILURE);

			FILE *f = fopen(fpath, "a");
			if (f == NULL) {
				perror("fopen");
				exit(EXIT_FAILURE);
			}
			FILE *tmpf = fopen(tmppath, "r");
			if (tmpf == NULL) {
				perror("fopen");
				exit(EXIT_FAILURE);
			}
			char *line = NULL;
			size_t len = 0;
			ssize_t nread;
			print_note_header(f, hour, minute, tagc, tagv);
			while ((nread = getline(&line, &len, tmpf)) != -1) {
				fprintf(f, line);
			}

			free(line);
			fclose(f);
			fclose(tmpf);

			if (remove(tmppath)) {
				perror("remove");
				exit(EXIT_FAILURE);
			}
			if (remove(tmpdir)) {
				perror("remove");
				exit(EXIT_FAILURE);
			}
		}
	}
}
