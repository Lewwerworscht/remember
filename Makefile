CC=gcc
CFLAGS=-Wall -Werror -pedantic
BIN=remember
SRC=remember.c

$(BIN): $(SRC)
	$(CC) $(CFLAGS) -o $(BIN) $(SRC)


debug:
	$(CC) $(CFLAGS) -o $(BIN) $(SRC) -ggdb
